# AW - Uso de GitFlow 

Esta [herramienta](http://danielkummer.github.io/git-flow-cheatsheet/index.es_ES.html#features) nos permitira tener mayor control de nuestro cambios, evitar conflictos con versiones que esten trabajando otra persona y gestionar mejor los releases que se hagan para el QA o Producción.

Recomiendo mucho usar [GitKraken( Cliente GUI Git con soporte de GitFlow)](https://www.gitkraken.com/).

## Requisitos

* Tener instalado Git (ya tiene incluido GitFlow)
* Cliente Git GUI (En este caso usremos GitKraken)


## Descarga

Podemos descargar desde [este enlace](https://www.gitkraken.com/download/windows).

## Configuración

1. Nos posicionamos en la carpeta que contiene el repositorio e incializamos gitflow con el siguiente comando:

`git flow init`

<img src="images/gitflow1.jpg" width="600">

2. Abrimos el GitKraken y nos aparecera el menu de gitflow.

<img src="images/gitflow2.jpg" width="600">

## Propuesta

<img src="images/gitflow6.jpg" width="600">

Tambien seria bueno tener manejar las versiones con este estandar semantico:

     1  .  0   .  2

Major |  Menor | Patch

* __Major__: Cambio drástico en el software. No es compatible con código hecho en versiones anteriores.
* __Minor__: Cambio que añade alguna característica nueva al software o modifica alguna ya existente, pero que sigue siendo compatible con código existente. También cuando marcamos algo como obsoleto.
* __Patch__: Cuando arreglamos un bug siendo el cambio retrocompatible.

Estas versiones seran los nombres (tags) de las ramas releases que deployaremos. 